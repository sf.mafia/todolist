import React, { Component } from 'react';
import {BrowserRouter, Route} from 'react-router-dom';
import Navbar from './components/Navbar';
import Home from './components/Home';
import Complete from './components/Complete';
import Trash from './components/Trash';
import Create from './components/Create';

class App extends Component {

  render() {
    return (
      <BrowserRouter>
        <Navbar />
        <Route exact path='/' component={Home} />
        <Route path='/Complete' component={Complete} />
        <Route path='/Trash' component={Trash} />
        <Route path='/Create' component={Create} />
      </BrowserRouter>
      
    )
  }
}

export default App;
