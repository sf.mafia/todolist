import React from 'react';
import EditTodo from './EditTodo';
import CreateTodo from './CreateTodo';
import { Redirect } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { redirectTrue, redirectFalse } from '../reduces/rootReducer'

const Create = () => {

    const navigate = useSelector(state => state.navigate);
    const redirect = useSelector(state => state.redirect);
    const dispatch = useDispatch();

    if (!navigate) {
        dispatch(redirectFalse())
    }
    
    return (
        <div className="container">
            {navigate ? <CreateTodo /> : <EditTodo /> }
        </div> 
    )
}

export default Create