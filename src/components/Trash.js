import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateRestore } from '../reduces/rootReducer';
import { useHistory } from "react-router-dom";
import { Redirect } from 'react-router';

const Trash = () => {
    const deleteItem = useSelector(state => state.deleteItem);
    const redirect = useSelector(state => state.redirect);
    const history = useHistory();
    const dispatch = useDispatch();

    if (!redirect) {
        alert('You must confirm button first!');
        history.push("/Create");
    }

    const handleRestore = (id) => {
        dispatch(updateRestore(id));
    }
    
    const item = deleteItem.map(item => {
        return (
                <tr key={item.id}>
                    <td>
                        <span>{item.content} </span>
                    </td>
                    <td>
                        <span>{item.dueDate} </span>
                    </td>
                    <td>
                        <span>{item.dateCompleted} </span>
                    </td>
                    <td>
                        <button 
                            onClick={() => handleRestore(item.id)}
                            className="btn-small light-green darken-2"
                        >X</button>
                    </td>
                </tr>
        )
    })
    const textTitle =  <tr className="text-title"><td colSpan="4">sorry, there's nothing here</td></tr>;

    return (
        <div className="container">
            <table className="responsive-table" >
                <thead>
                    <tr>
                        <th>Content</th>
                        <th>Due Date</th>
                        <th>Date Completed</th>
                        <th>Restore</th>
                    </tr>
                </thead>
                <tbody className="tbody-custom">
                    {item.length === 0 ? textTitle : item}
                </tbody>
            </table>
        </div>
    )
}

export default Trash