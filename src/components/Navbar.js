import React from 'react'
import {Link} from 'react-router-dom'

const Navbar = () => {
    return (
        <nav className="nav nav-wrapper red darken-3">
            <div className="container">
                <ul className="right">
                    <li><Link to="/">Home</Link></li>
                    <li><Link to="/Complete">Complete</Link></li>
                    <li><Link to="/Trash">Trash</Link></li>
                    {/* <li><Link to="/Create">Create</Link></li> */}
                </ul>
            </div>
        </nav>
    )
}

export default Navbar