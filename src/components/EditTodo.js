import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { updateEditTodo } from '../reduces/rootReducer';
import { Redirect } from 'react-router';
import { useHistory } from "react-router-dom";
import { redirectTrue, redirectFalse } from '../reduces/rootReducer';

const EditTodo = () => {

    const page = useSelector(state => state.page);
    const redirect = useSelector(state => state.redirect);
    const history = useHistory();
    const editTodos = useSelector(state => state.editTodos);
    const dispatch = useDispatch();
    const [state, setState] = useState(editTodos.map(item=> item.content)); 
    const item = editTodos.map(item=>item.id); 

    const handleChange = (e) => {
        setState(e.target.value);
    };
    const handleEdit = (e) => {
        e.preventDefault();
        const data = {
            id: item.toString(),
            content: e.target[0].value,
            dueDate: new Date().toLocaleDateString(),
            dateCompleted: new Date().toLocaleDateString(),
            complete: false
        }
        dispatch(updateEditTodo(data));
        e.target[0].value = '';
        dispatch(redirectTrue());
        history.push("/");
    };

    return ( 
        <form onSubmit={(e) => handleEdit(e)}>
            <input type="text" value={state} onChange={(e) => handleChange(e)}/>
            <button type="submit" className="btn-small red lighten-2">Confirm</button>
        </form>
        );
    }

 
export default EditTodo;