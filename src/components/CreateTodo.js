import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { createTodo } from '../reduces/rootReducer';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { useHistory } from "react-router-dom";


const CreateTodo = () => {
    
    const todos = useSelector(state => state.todos);
    const count = useSelector(state => state.count);
    const history = useHistory();
    //using dispatch
    const dispatch = useDispatch();
    //set state of Date
    const [startDate, setStartDate] = useState(new Date());
    
    const handleCreate = (e) => {
        e.preventDefault();
        if(e.target[0].value !== ''){
            const data = {
                id: count + 1,
                content: e.target[0].value,
                dueDate: startDate.toLocaleDateString(),
                dateCompleted: new Date().toLocaleDateString(),
                complete: false,
                count: count+1
            }
            dispatch(createTodo(data))
            e.target[0].value = '';
            history.push("/");
        }else {
            alert('Please fill out the form below:');
        }
    }

    return (
        <div>
            <h3>Create Todo:</h3>
            <form onSubmit={(e) => handleCreate(e)} className="row">
                <div className="row">
                    <div className="col s12">
                        <input type="text" placeholder="Type something..."/>
                    </div>
                </div>
                <div className="row">
                    <div className="col s6">
                        Due Date: 
                        <DatePicker selected={startDate} onChange={date => setStartDate(date)} className="datapicker"
                    />
                    </div>
                </div>
                <div className="row">
                    <div className="col 12">
                        <button type="submit" className="btn-small red lighten-2">Create</button>
                    </div>
                </div>
            </form>
        </div>
    )
}

export default CreateTodo